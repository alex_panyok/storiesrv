package com.testrv.android

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet


class StoriesRecycleView(context: Context, attrs: AttributeSet?, defStyle: Int) : RecyclerView(context, attrs, defStyle) {

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context) : this(context, null, 0)


    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        change()
    }




    private fun getLLayoutManager(): LinearLayoutManager {
        return if (layoutManager is LinearLayoutManager && (layoutManager as LinearLayoutManager).orientation == LinearLayoutManager.HORIZONTAL) {
            layoutManager as LinearLayoutManager
        } else {
            throw IllegalStateException("LayoutManager must be linear with horizontal orientation")
        }
    }

    private fun change() {
        val firstView = getLLayoutManager().findViewByPosition(getLLayoutManager().findFirstVisibleItemPosition())
        val lastView = getLLayoutManager().findViewByPosition(getLLayoutManager().findLastVisibleItemPosition())

        val scaleFirst = 1f - .2f * (Math.abs(firstView.x) / firstView.width)
        firstView.scaleX = scaleFirst
        firstView.scaleY = scaleFirst

        val scaleSecond = 1f - .2f * (Math.abs(lastView.x) / lastView.width)
        lastView.scaleX = scaleSecond
        lastView.scaleY = scaleSecond
    }


}



