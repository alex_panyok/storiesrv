package com.testrv.android

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*


class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val list: ArrayList<Int> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    fun setItems(list: List<Int>) {
        this.list.clear()
        this.list.addAll(list)
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setBackgroundColor(Color.argb(255,
                    Random().nextInt(256),
                    Random().nextInt(256),
                    Random().nextInt(256)
            ))
        }
    }
}